import { Component, HostBinding, OnInit } from '@angular/core';
import { CardComponent } from '../card/card.component';
import { Cat } from '../shared/models/cat.model';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from '../details/details.component';
import { RouterOutlet } from '@angular/router';
import { CatService } from '../shared/services/cat.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from '../shared/services/auth.service';
import { switchMap } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { FilterComponent } from '../filter/filter.component';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [CardComponent, SharedModule, CommonModule, DetailsComponent, RouterOutlet, HttpClientModule, FilterComponent],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent implements OnInit{
  @HostBinding('class') class = 'app'
  cats!: Cat[];
  name!: string;
  pageSlice!: Cat[];
  breeds: string[] = [];
  filteredCats: Cat[] = [];
  constructor(
    private catService: CatService,
    private authService: AuthService
  ){}

  ngOnInit(){
    this.authService.user.pipe(switchMap((res: string | null)=>{ 
      if(res === null){
        return this.catService.getBasicCats()
      } else {
        return this.catService.getPremiumCats()
      }
    }
    )).subscribe(res => {
      this.cats = res;
      this.breeds = Array.from(
        new Set(this.cats.map(cat => {
          return cat.breed
        }))
      );
      this.pageSlice = this.cats.slice(0, 6)
    })
  }

  onPageChange(event: PageEvent) {
    console.log(event);
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;
    if (endIndex > this.cats?.length) {
      endIndex = this.cats?.length;
    }
     this.pageSlice = this.cats.slice(startIndex, endIndex);
  }

  onSubmitFilters(values: any){
    this.filteredCats = this.cats.filter(cat => {
      return (
        (values.name.length === 0 || cat.name.toLowerCase().includes(values.name.toLowerCase())) &&
        (values.age.length === 0 || cat.age.includes(values.age)) &&
        (values.breed === '' || cat.breed === values.breed) &&
        ((!values.isAdopted.yes && !values.isAdopted.no) ||
        (values.isAdopted.yes && cat.is_adopted === true) ||
        (values.isAdopted.no && cat.is_adopted === false)) 
      )
    });
    this.pageSlice = this.filteredCats.slice(0, 5)
  }

}
