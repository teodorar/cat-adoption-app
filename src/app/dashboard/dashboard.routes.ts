import { Routes } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";
import { DetailsComponent } from "../details/details.component";

export const ADOPT_ROUTES: Routes = [
    {
        path: '',
        component: DashboardComponent,
    },
    {
        path: ':id',
        component: DetailsComponent
    }
];