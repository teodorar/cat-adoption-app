import { Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';

export const routes: Routes = [
    {
        path: 'adopt',
        loadChildren: () => import('./dashboard/dashboard.routes').then(r => r.ADOPT_ROUTES)
    },
    {
        path: 'login',
        component: LoginComponent
    }
];
