import { Component, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { EventEmitter } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-filter',
  standalone: true,
  imports: [ReactiveFormsModule, SharedModule, CommonModule],
  templateUrl: './filter.component.html',
  styleUrl: './filter.component.css'
})
export class FilterComponent implements OnInit {
  @Input() breeds: string[]= [];
  @Output() submitFilters = new EventEmitter<any>();
  loggedInUser!: string | null;
  filterForm = this.fb.group({
    name: '',
    age: '',
    breed: [''],
    isAdopted: this.fb.group({
      yes: [false],
      no: [false]
    })
  });

  constructor(private fb: FormBuilder,
    private authService: AuthService
  ){}

  ngOnInit(){
    this.authService.user.subscribe(res => this.loggedInUser = res)
  }

  onFilter(){
    this.submitFilters.emit(this.filterForm.value);
    console.log(this.filterForm.value)
  }
}
