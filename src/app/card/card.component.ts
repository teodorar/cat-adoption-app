import { Component, Input, OnInit } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule, SharedModule],
  templateUrl: './card.component.html',
  styleUrl: './card.component.css'
})
export class CardComponent implements OnInit{
  @Input() id!: string;
  @Input() name!: string;
  @Input() age!: string;
  @Input() breed!: string;
  @Input() image!: string;
  @Input() adopted!: boolean | null | undefined;
  loggedInUser!: string | null;
  constructor(private authService: AuthService){}

  ngOnInit(){
    this.authService.user.subscribe(res => this.loggedInUser = res)
  }
}
