import { Component, OnInit } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ActivatedRoute } from '@angular/router';
import { CatService } from '../shared/services/cat.service';
import { Cat } from '../shared/models/cat.model';
import { CommonModule } from '@angular/common';
import { AuthService } from '../shared/services/auth.service';
import { mergeMap, switchMap } from 'rxjs';

@Component({
  selector: 'app-details',
  standalone: true,
  imports: [CommonModule, SharedModule],
  templateUrl: './details.component.html',
  styleUrl: './details.component.css'
})
export class DetailsComponent implements OnInit{
  cat!: Cat;
  loggedInUser!: string | null;
  constructor(
    private activatedRoute: ActivatedRoute,
    private catService: CatService,
    private authService: AuthService
  ){
  }

  ngOnInit() {
    this.authService.user.pipe(switchMap((res: string | null)=>{ 
      this.loggedInUser = res;
      if(res === null){
        return this.catService.getBasicCat(this.activatedRoute.snapshot.paramMap.get('id'))
      } else {
        return this.catService.getPremiumCat(this.activatedRoute.snapshot.paramMap.get('id'))
      }
    }
    )).subscribe(res => {
      this.cat = res;
    })
  }
}
