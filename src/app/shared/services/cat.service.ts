import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cat } from '../models/cat.model';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CatService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getBasicCats(){
    return this.httpClient.get<Cat[]>('https://66101e9b0640280f219c53ec.mockapi.io/api/v1/cats/cats')
  }

  getBasicCat(id: string | null){
      return this.httpClient.get<Cat>(`https://66101e9b0640280f219c53ec.mockapi.io/api/v1/cats/cats/${id}`)
  }

  getPremiumCats(){
    return this.httpClient.get<Cat[]>('https://66101e9b0640280f219c53ec.mockapi.io/api/v1/cats/premium-cats', { headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Basic ' + localStorage.getItem('loggedInUser')
    })})
  }

  getPremiumCat(id: string | null){
    return this.httpClient.get<Cat>(`https://66101e9b0640280f219c53ec.mockapi.io/api/v1/cats/premium-cats/${id}`, { headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Basic ' + localStorage.getItem('loggedInUser')
    })})
}
}
