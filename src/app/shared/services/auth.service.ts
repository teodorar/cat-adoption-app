import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // user;
  // constructor(@Inject(PLATFORM_ID) private platformId: Object) {
  //   if (isPlatformBrowser(this.platformId)) {
  //      this.user = new BehaviorSubject<string | null>(
  //       JSON.parse(localStorage.getItem('loggedInUser') || 'null')
  //     );
  //   }
  // }
  // localStorage = this.document.defaultView?.localStorage;
  user = new BehaviorSubject<string | null>(localStorage.getItem('loggedInUser'))


  login(email: string | undefined | null, password: string | undefined | null){
    const token = btoa(email + ':' + password);
    localStorage.setItem('loggedInUser', btoa(email + ':' + password));
    this.user.next(token)
  }

  logout(){
    localStorage.clear();
    // localStorage.setItem('loggedInUser', 'null');
      this.user.next(null)
  }
}
