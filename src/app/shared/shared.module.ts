import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import {MatListModule} from '@angular/material/list';
import { RouterLink } from '@angular/router';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterLink, MatToolbarModule, MatButtonModule, MatDividerModule, MatCardModule, MatChipsModule, MatListModule, MatPaginatorModule, FormsModule, MatSelectModule, MatCheckboxModule, MatMenuModule
  ],
  exports: [CommonModule, RouterLink, MatToolbarModule, MatButtonModule, MatDividerModule, MatCardModule, MatChipsModule, MatListModule, MatPaginatorModule, FormsModule, MatSelectModule, MatCheckboxModule, MatMenuModule]
})
export class SharedModule { }
