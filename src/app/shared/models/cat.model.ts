export interface Cat {
    id: string,
    listed_at: Date,
    name: string,
    image: string,
    age: string,
    breed: string,
    description: string,
    contact_name: string,
    contact_phone: string,
    adoption_fee: number | null,
    is_adopted: boolean | null,
}