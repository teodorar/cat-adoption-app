import { CommonModule, isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnChanges, OnInit, PLATFORM_ID } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';



@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, SharedModule],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent implements OnInit {
  user!: string | null;
  constructor(
    private router: Router,
    private authService: AuthService
  ){}
  
  ngOnInit(){
    this.authService.user.subscribe(res => this.user = res)
  }
  logout(){
    this.authService.logout();
    this.authService.user.subscribe(res => this.user = res)
    this.router.navigate(['/adopt'])
  }
}
